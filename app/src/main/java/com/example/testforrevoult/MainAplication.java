package com.example.testforrevoult;

import android.app.Application;
import com.example.testforrevoult.component.ApplicationModule;
import com.example.testforrevoult.component.DaggerRestComponent;
import com.example.testforrevoult.component.RestComponent;

public class MainAplication extends Application {

    private RestComponent repositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        repositoryComponent = DaggerRestComponent.builder()
                .applicationModule(new ApplicationModule((getApplicationContext())))
                .build();
    }

    public RestComponent getRestComponent() {
        return repositoryComponent;
    }
}