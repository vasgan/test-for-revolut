package com.example.testforrevoult.main

data class Currency(val name : String, var value: Float, val factor: Float, val select: Boolean)