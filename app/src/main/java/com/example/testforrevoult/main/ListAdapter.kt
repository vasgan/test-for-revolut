package com.example.testforrevoult.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.testforrevoult.R

class ListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val EDIT_HOLDER = 0
        const val VIEW_HOLDER = 1
    }

    var list = ArrayList<Currency>()
    lateinit var callbackEdit: CallbackEdit
    lateinit var callbackClick: CallbackClick
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {

        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_list, p0, false)
        return if (p1 == EDIT_HOLDER)
            EditVIewHolder(v)
        else BaseViewHolder(v)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0)
            EDIT_HOLDER
        else
            VIEW_HOLDER
    }
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p0 is BaseViewHolder) {
            p0.initView(list[p1], callbackClick)
        }
        else if (p0 is EditVIewHolder) {
            p0.initView(list[p1], callbackEdit)
        }
    }
}