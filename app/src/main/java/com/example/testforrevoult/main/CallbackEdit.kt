package com.example.testforrevoult.main

interface CallbackEdit {
    fun onEditView(edit: String)
}