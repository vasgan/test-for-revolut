package com.example.testforrevoult.main

import com.example.testforrevoult.BasePresenter
import com.example.testforrevoult.BaseView
import com.example.testforrevoult.component.network.response.ResponseList

interface MainContract {
    interface MainView: BaseView<Presenter> {
        fun showItems(list: ResponseList)
    }
    interface Presenter : BasePresenter {
        fun loadItems(currency: String)
    }
}