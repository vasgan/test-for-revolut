package com.example.testforrevoult.main.di;

import com.example.testforrevoult.main.MainContract;
import dagger.Module;
import dagger.Provides;

@Module
public class MainPresenterModule {
    private final MainContract.MainView view;

    public MainPresenterModule(final MainContract.MainView view) {
        this.view = view;
    }

    @Provides
    MainContract.MainView provideProduct() {
        return view;
    }
}
