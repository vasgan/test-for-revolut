package com.example.testforrevoult.main

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.example.testforrevoult.R

class BaseViewHolder(
    itemView: View?,
    private val edit: EditText? = itemView?.findViewById(R.id.edit_text),
    private val name: TextView? = itemView?.findViewById(R.id.name_text)
) : RecyclerView.ViewHolder(itemView!!) {
    fun initView(item: Currency,callbackClick: CallbackClick
    ) {
        name?.text = item.name
        edit?.setText((item.factor * item.value).toString())
        edit?.isEnabled = false
        itemView.setOnClickListener {
            callbackClick.click(item.name, item.factor * item.value)
        }
    }
}
