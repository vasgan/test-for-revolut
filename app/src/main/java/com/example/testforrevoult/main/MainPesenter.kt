package com.example.testforrevoult.main

import com.example.testforrevoult.component.network.response.RestRequests
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainPesenter @Inject constructor(var view: MainContract.MainView) : MainContract.Presenter {

    @Inject
    lateinit var productRest: RestRequests
    var sub : Disposable? = null

    override fun loadItems(currency: String) {
        sub?.dispose()
        sub = productRest.getCurrency(currency).repeatWhen {
            it.delay(1, TimeUnit.SECONDS) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ t ->
                view.showItems(t)
            }, {
            }, {
            })
    }
}