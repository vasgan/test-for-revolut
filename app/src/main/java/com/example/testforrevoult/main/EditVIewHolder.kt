package com.example.testforrevoult.main

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.example.testforrevoult.R

class EditVIewHolder(
    itemView: View?,
    private val edit: EditText? = itemView?.findViewById(R.id.edit_text),
    private val name: TextView? = itemView?.findViewById(R.id.name_text)
) : RecyclerView.ViewHolder(itemView!!) {
    lateinit var callbackEdit: CallbackEdit

    fun initView(item: Currency, callbackEdit: CallbackEdit) {
        this.callbackEdit = callbackEdit
        name?.text = item.name
        edit?.setText((item.factor * item.value).toString())
        edit?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                callbackEdit.onEditView(edit.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
    }
}
