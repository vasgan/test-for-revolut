package com.example.testforrevoult.main.di;

import com.example.testforrevoult.component.RestComponent;
import com.example.testforrevoult.main.MainActivity;
import com.example.testforrevoult.utils.ActivityScoped;
import dagger.Component;

@ActivityScoped
@Component(dependencies = RestComponent.class, modules = MainPresenterModule.class)
public interface MainComponent {
    void inject(MainActivity productActivity);
}
