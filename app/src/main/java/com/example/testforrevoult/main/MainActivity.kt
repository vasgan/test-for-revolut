package com.example.testforrevoult.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.testforrevoult.MainAplication
import com.example.testforrevoult.R
import com.example.testforrevoult.component.network.response.ResponseList
import com.example.testforrevoult.main.di.DaggerMainComponent
import com.example.testforrevoult.main.di.MainPresenterModule
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainContract.MainView {
    var value: Float = 1.0f
    private val result = ArrayList<Currency>()
    @Inject
    lateinit var presenter: MainPesenter

    val adapter = ListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerMainComponent.builder()
            .restComponent((application as MainAplication).restComponent)
            .mainPresenterModule(MainPresenterModule(this))
            .build()
            .inject(this)
        val layoutManager = LinearLayoutManager(this)
        list.layoutManager = layoutManager
        adapter.callbackClick = object : CallbackClick{
            override fun click(text: String, v: Float) {
                value = v
                presenter.loadItems(text)
            }
        }
        adapter.callbackEdit = object : CallbackEdit {
            override fun onEditView(edit: String) {
                value = edit.toFloat()
                for (i in result.indices) {
                    if (i != 0) {
                        result[i].value = value
                        list.post { adapter.notifyItemChanged(i) }
                    }
                }
            }
        }
        list.adapter = adapter
        presenter.loadItems("EUR")
    }
    override fun showItems(list: ResponseList) {
        result.clear()
        result.add(Currency(list.base, value, 1.0f, true))
        for (i in list.rates.toList()) {
            result.add(Currency(i.first, value, i.second.toFloat(), false))
        }
        adapter.list = result
        adapter.notifyDataSetChanged()
    }
}
