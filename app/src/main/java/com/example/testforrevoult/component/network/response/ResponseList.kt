package com.example.testforrevoult.component.network.response

data class ResponseList(val base: String, val date: String, val rates: Map<String, String>)