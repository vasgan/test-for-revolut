package com.example.testforrevoult.component;

import com.example.testforrevoult.component.network.response.RestRequests;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {ApplicationModule.class, RetrofitModule.class})
public interface RestComponent {

    RestRequests getProductRest();

    RestApi getRetrofit();
}
