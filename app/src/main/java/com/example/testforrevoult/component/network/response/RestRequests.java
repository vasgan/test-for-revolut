package com.example.testforrevoult.component.network.response;

import com.example.testforrevoult.component.RestApi;
import io.reactivex.Observable;

import javax.inject.Inject;

public class RestRequests {
    private RestApi retrofit;
    @Inject
    public RestRequests(RestApi retrofit) {
        this.retrofit = retrofit;
    }

    public Observable<ResponseList> getCurrency(String currency) {
        return retrofit.getList(currency);
    }
}
