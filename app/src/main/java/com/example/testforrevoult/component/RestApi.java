package com.example.testforrevoult.component;

import com.example.testforrevoult.component.network.response.ResponseList;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {
    @GET("latest")
    Observable<ResponseList> getList(@Query("base") String currency);
}
