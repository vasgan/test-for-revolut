package com.example.testforrevoult.component;

import android.content.Context;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule  {
    private Context context;

    public ApplicationModule(final Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext(){
        return context;
    }
}
