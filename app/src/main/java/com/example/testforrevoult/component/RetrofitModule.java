package com.example.testforrevoult.component;

import android.content.Context;
import com.example.testforrevoult.R;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.inject.Singleton;

@Module
public class RetrofitModule {

    @Provides
    protected RestApi provideApiClient(Retrofit retrofit) {
        return retrofit.create(RestApi.class);
    }

    @Singleton
    @Provides
    protected Retrofit provideRetrofit(Context context, OkHttpClient client) {
        final String baseUrl = context.getString(R.string.base_url);

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    @Singleton
    @Provides
    protected OkHttpClient provideOkHttpClient(Context context, HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .retryOnConnectionFailure(true).build();
    }

    @Singleton
    @Provides
    protected HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }
}
